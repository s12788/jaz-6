package domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@Entity
@NamedQueries({
@NamedQuery(name="product.all", query="SELECT p FROM Product p"),
@NamedQuery(name="product.id", query="SELECT p FROM Product p WHERE p.id=:productId"),
})

public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	@ManyToOne/*(cascade = CascadeType.ALL)*/
	private Category category;
	private List<Comment> comments = new ArrayList<Comment>();
	private float price;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	@XmlTransient
	@OneToMany(mappedBy="product", cascade = CascadeType.ALL, orphanRemoval=true)
	public List<Comment> getComment() {
		return comments;
	}
	public void setComment(List<Comment> comment) {
		this.comments = comment;
	}
	

}
