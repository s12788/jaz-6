	
	package rest;
	

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import domain.*;
	
	@Path("/products")
	@Stateless
	public class ProductsResources {
	
//		private ProductService db = new ProductService();
		
		@PersistenceContext
		EntityManager em;
		
		@GET
//		@Path("/products")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> getAll()
		{
			return em.createNamedQuery("product.all", Product.class).getResultList();
		}
		
		@POST
		@Path("/categories")		
		@Consumes(MediaType.APPLICATION_JSON)
		public Response add(Category category){
			em.persist(category);
			return Response.ok(category.getId()).build();
		}
		
//		@POST
//		@Path("/products")
//		@Consumes(MediaType.APPLICATION_JSON)
//		public Response Add(Product product){
//			em.persist(product);
//			return Response.ok(product.getId()).build();
//		}
		
		@POST
		@Path("/categories/{id}")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response addProduct(@PathParam("id") int categoryId, Product product){
			Category result = em.createNamedQuery("category.id", Category.class)
					.setParameter("categoryId", categoryId)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
				result.getProducts().add(product);
				product.setCategory(result);
				em.persist(product);
			return Response.ok().build();
		}
		
		@POST
		@Path("/{id}/comment")		
		@Consumes(MediaType.APPLICATION_JSON)
		public Response add(@PathParam("id") int productId, Comment comment){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", productId)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
				result.getComment().add(comment);
				comment.setProduct(result);
				em.persist(comment);
			return Response.ok().build();
		}
			
		@GET
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response get(@PathParam("id") int id){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
		
		@PUT
		@Path("/{id}")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response update(@PathParam("id") int id, Product p){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null){
				return Response.status(404).build();
			}
			result.setName(p.getName());
//			result.setCategory(p.getCategory());
			result.setPrice(p.getPrice());
			em.persist(result);
			return Response.ok(result).build();
		}
		@DELETE
		@Path("{id}")
		public Response delete(@PathParam("id") int id){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null){
				return Response.status(404).build();
			}
			
			Category category = em.createNamedQuery("category.id", Category.class)
					.setParameter("categoryId", result.getCategory().getId())
					.getSingleResult();
			
			List <Comment> comments = em.createNamedQuery("comment.all", Comment.class).getResultList();
			
			Iterator<Comment> c = comments.iterator();
			Comment comment;
			while(c.hasNext()){
				comment = c.next();
				if (comment.getProduct().getId()==id) 
				comment.setProduct(null);
//				c.remove();
			}
	
			
//			category.getProducts().remove(id-1);
			
			List<Product> productThisCategory = category.getProducts();
			Iterator<Product> i = productThisCategory.iterator();
			Product product;
			while(i.hasNext()){
				product = i.next();
				if (product.getId()==id) i.remove();
			}
			
//			List <Comment> comments = result.getComment();
//			Iterator<Comment> c = comments.iterator();
//			Comment comment;
//			while(c.hasNext()){
//				comment = c.next();
//				if (comment.getId()==id) comment.setProduct(null);
//			}
			
			
			em.remove(result);
			return Response.ok(result).build();
		}
		
		@DELETE
		@Path("/{productId}/comment/{commentId}")
		public Response delete(@PathParam("productId") int productId,
				@PathParam("commentId") int commentId){
			Product product = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", productId)
					.getSingleResult();
			Comment comment = em.createNamedQuery("comment.id", Comment.class)
					.setParameter("commentId", commentId)
					.getSingleResult();
			
			if(product==null || comment==null){
				return Response.status(404).build();
			}
			
			List<Comment> commentThisProduct = product.getComment();
			Iterator<Comment> i = commentThisProduct.iterator();
			Comment c=null;
			while(i.hasNext()){
				c = i.next();
				if (c.getId()==commentId) i.remove();
			}

			em.remove(comment);
			return Response.ok(comment).build();
		}
		
		
//		@GET
//		@Path("/categoryId")
//		@Produces(MediaType.APPLICATION_JSON)
//		public Response getProducts(@PathParam("categoryId") int categoryId){
//			Category result = em.createNamedQuery("category.id", Category.class)
//					.setParameter("categoryId", categoryId)
//					.getSingleResult();
//			if(result==null)
//				return Response.status(404).build();
//			return Response.ok(result.getProducts()).build();
//		}
		
		@GET
		@Path("/categories/{categoryId}")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> getProducts(@PathParam("categoryId") int categoryId){
			Category result = em.createNamedQuery("category.id", Category.class)
					.setParameter("categoryId", categoryId)
					.getSingleResult();
			if(result==null)
				return null;
			return result.getProducts();
		}
		
		@GET
		@Path("/{id}/comment")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Comment> getComments(@PathParam("id") int id){
			Product result = em.createNamedQuery("product.id", Product.class)
					.setParameter("productId", id)
					.getSingleResult();
			if(result==null)
				return null;
			return result.getComment();
		}
		
		@GET
		@Path("/search/{name}")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> get(@PathParam("name") String name){
			
			List<Product> products = em.createNamedQuery("product.all", Product.class).getResultList();
			List<Product> productsByName = new ArrayList<Product>();
			String pattern = ".*"+name+".*";
			Pattern r = Pattern.compile(pattern);
			Matcher m; 
			for(Product p: products){
				m = r.matcher(p.getName());
					if(m.find()){
						productsByName.add(p);
					}
			}
			return productsByName;
		}
		
		@GET
		@Path("/search/{lowestPrice}/{highestPrice}")
		@Produces(MediaType.APPLICATION_JSON)
		public List<Product> get(@PathParam("lowestPrice") int lowestPrice,
				@PathParam("highestPrice") int highestPrice){
			
			List<Product> products = em.createNamedQuery("product.all", Product.class).getResultList();
			List<Product> productsByPrice = new ArrayList<Product>(); 
			for(Product p: products){
					if(p.getPrice()>=lowestPrice && p.getPrice()<=highestPrice){
						productsByPrice.add(p);
					}
			}
			return productsByPrice;
		}
		
//		@POST
//		@Path("/{id}/comment")		
//		@Consumes(MediaType.APPLICATION_JSON)
//		public Response add(@PathParam("id") int productId, Comment comment){
//			Product result = em.createNamedQuery("product.id", Product.class)
//					.setParameter("productId", productId)
//					.getSingleResult();
//			if(result==null)
//				return Response.status(404).build();
//				result.getComment().add(comment);
//				comment.setProduct(result);
//				em.persist(comment);
//			return Response.ok().build();
//		}
		
		
		
	}
	
	
	
	

	